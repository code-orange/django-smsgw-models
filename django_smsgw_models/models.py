from datetime import datetime

from phonenumber_field.modelfields import PhoneNumberField

from django_mdat_customer.django_mdat_customer.models import *


class SmsGwMessage(models.Model):
    customer = models.ForeignKey(MdatCustomers, models.CASCADE)
    destination_nr = PhoneNumberField()
    text = models.TextField()
    sent = models.BooleanField(default=False)
    added_date = models.DateTimeField(default=datetime.now)
    sent_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = "smsgw_message"
